      module sd_interfaces
        
      interface

      ! tdc 2017-04-04
      ! Some of these subroutine prototypes will probably need to move to more relevent modules
      subroutine actions (id, ob_num, sd)
      use sd_hru_module
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      integer :: id, ob_num
      end subroutine
      
      subroutine cal_chsed (sd_db, sd_init, sd)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd_init, sd
      end subroutine

      subroutine cal_hyd (sd_db, sd)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine
     
      subroutine cal_plant (sd_db, sd_init, sd)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd_init, sd
      end subroutine
      
      subroutine cal_sed (sd_db, sd)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine
      
      subroutine calt_hyd (sd_db, sd_init, sd)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd_init, sd
      end subroutine

      subroutine codes_cal_read (sd_init)
      use sd_hru_module
      type(swatdeg_hru_dynamic), allocatable, dimension (:), intent(inout) :: sd_init
      end subroutine
     
      subroutine command (sd_db, sd)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine
      
      subroutine deallocate_main (sd_db,sd,sd_init)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd, sd_init
      end subroutine

      subroutine current_par_value(ielem, ly, chg_parm, chg_typ, chg_val, absmin, absmax, num_db, sd_db)
      use sd_hru_module
      character(len=16), intent (in) :: chg_parm, chg_typ
      real, intent (in) :: chg_val, absmin, absmax
      integer, intent (in) :: ielem, num_db, ly
      type(swatdeg_hru_data), dimension (:), intent(inout) :: sd_db
      end subroutine
      
      subroutine dr_sub (sd_db)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      end subroutine

      subroutine lcu_softcal_read (sd)
      use sd_hru_module
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine
      
      subroutine sd_hru_control(isd,sd_db,sd)
      use sd_hru_module
      integer :: isd
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine
      
      subroutine sd_hru_output (isd, isd_db, sd_db, sd)
      use sd_hru_module
      integer :: isd, isd_db
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine

      subroutine sd_hru_read (sd_db, sd)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(inout) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine
      
      subroutine subbasin_control(sd_db)
      use sd_hru_module
      type(swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      end subroutine
      
      subroutine time_control (sd_db,sd)
      use sd_hru_module
      type (swatdeg_hru_data), allocatable, dimension (:), intent(in) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine

      subroutine update_init(sd_db,sd)
      use sd_hru_module
      type(swatdeg_hru_data), dimension (:), intent(inout) :: sd_db
      type (swatdeg_hru_dynamic), allocatable, dimension (:) :: sd
      end subroutine
      
      end interface
      end module sd_interfaces
