      module sd_hru_module
      
      ! use hydrograph_module
      ! use time_module
      ! use climate_module
      ! use output_landscape_module
      ! use input_file_module
 
! tdc 2017-04-04      
!! these variables don't seem to be used anywhere      
      ! real :: a1, a2
      ! real :: precip, snowfall, snowmelt, runoff, flowlat, chflow, perc, flow_tile
      ! real :: tmax, tmin, tave, raobs, tstress, ws, strsair, biomass, phubase0
      ! real :: wndspd, rhum, air
      ! real :: cn_sd, aet, pet, sedin
      ! real, dimension(:), allocatable :: sd_qday, sd_qfdc, pr, be
!! these variables only used in sd_hru_read; moved there.      
      !real, dimension(12) :: awct = 0.
      !real, dimension(12) :: port = 0.
      !real, dimension(12) :: scon = 0.
        
      type swatdeg_hru_data
        character(len=16) :: name
        real :: dakm2 = 0.          !km^2          |drainage area
        real :: cn2 = 0.            !none          |condition II curve number
        real :: cn3_swf = 0.        !none          |soil water factor for cn3 (used in calibration)
                                    !              |0 = fc; 1 = saturation (porosity)
        real :: tc = 0.             !min           |time of concentration
        real :: soildep = 0.        !mm            |soil profile depth
        real :: perco = 0.          !              |soil percolation coefficient
        real :: slope = 0.          !m/m           |land surface slope
        real :: slopelen = 0.       !m             |land surface slope length
        real :: etco = 0.           !              |et coefficient - use with pet and aet
        real :: sy = 0.             !mm            |specific yld of the shallow aquifer
        real :: abf = 0.            !              |alpha factor groundwater
        real :: revapc = 0.         !              |revap coefficient amt of et from shallow aquifer
        real :: percc = 0.          !              |percolation coeff from shallow to deep
        real :: sw = 0.             !frac          |initial soil water (frac of awc)
        real :: gw = 0.             !mm            |initial shallow aquifer storage
        real :: gwflow = 0.         !mm            |initial shallow aquifer flow
        real :: gwdeep = 0.         !mm            |initital deep aquifer flow
        real :: snow = 0.           !mm            |initial snow water equivalent
        real :: xlat = 0.           !              |latitude
        integer :: itext = 0        !              |soil texture
                                    !              |1-sand 2-loamy sand 3-sandy loam 4-loam
                                    !              |5-silt loam 6-silt 7-silty clay 8-clay loam
                                    !              |9-sandy clay loam 10-sandy clay 
                                    !              |11-silty clay 12=clay 
        integer :: tropical         !              |0 = non-tropical; 1 = tropical;
        integer :: igrow1 = 0       !julian day    |start of growing season for non-tropical
                                    !              |start of monsoon initialization period for tropical
        integer :: igrow2 = 0       !julian day    |end of growing season for non-tropical
                                    !              |end of monsoon initialization period for tropical
        character(len=16) :: plant  !              |plant type (as listed in plants.plt)
        real :: stress = 0.         !frac          |plant stress - pest, root restriction, soil quality, nutrient, (non water, temp)
        integer :: ipet = 0         !              |potential ET method (0==Hargrove; 1==Priestley-Taylor)
        integer :: irr = 0          !              |irrigation code 0=no irr 1=irrigation
        integer :: irrsrc = 0       !              |irrigation source 0=outside basin 1=shal aqu 2=deep
        real :: tdrain = 0.         !hr            |design subsurface tile drain time
        real :: uslek = 0.          !              |usle soil erodibility factor
        real :: uslec = 0.          !              |usle cover factor
        real :: uslep = 0.          !none          |USLE equation support practice (P) factor
        real :: uslels = 0.         !none          |USLE equation length slope (LS) factor
      end type swatdeg_hru_data
      ! tdc 2017-04-04
      ! type (swatdeg_hru_data), dimension (:), allocatable :: sd_db
      
      type swatdeg_hru_dynamic
        character(len=16) :: name
        integer :: props
        integer :: obj_no
        character(len=16) :: region
        character(len=16) :: plant           !              |plant type (as listed in plants.plt)
        integer :: iplant = 1                !              |plant number xwalked from sd_db()%plant and plants.plt
        real :: km2 = 0.                     !km^2          |drainage area
        real :: cn2 = 0.                     !              |condition II curve number (used in calibration)
        real :: cn3_swf = 0.                 !none          |soil water factor for cn3 (used in calibration)
                                             !              |0 = fc; 1 = saturation (porosity)
        real :: etco = 0.                    !              |et coefficient - use with pet and aet (used in calibration)
        real :: revapc = 0.                  !m/m           |revap from aquifer (used in calibration)
        real :: perco = 0.                   !              |soil percolation coefficient (used in calibration)
        real :: tdrain = 0.                  !hr            |design subsurface tile drain time (used in calibration)
        real :: stress = 0.                  !frac          |plant stress - pest, root restriction, soil quality, nutrient, 
                                             !              |(non water, temp) (used in calibration)
        real :: uslefac = 0.                 !              |USLE slope length factor
        real :: wrt1 = 0.
        real :: wrt2 = 0.
        real :: smx = 0.
        real :: hk = 0.
        real :: yls = 0.
        real :: ylc = 0.
        real :: awc = 0.                     !mm/mm        |available water capacity of soil 
        real :: g = 0.
        real :: hufh = 0.
        real :: phu = 0.     
        real :: por = 0.
        real :: sc = 0.
        real :: sw = 0.                      !mm/mm         |initial soil water storage
        real :: gw = 0.                      !mm            |initial shallow aquifer storage
        real :: snow = 0.                    !mm            |initial water content of snow
        real :: gwflow = 0.                  !mm            |initial groundwater flow
        integer :: igro = 0                  !              |0=plant growing; 1=not growing
        real :: dm = 0.                      !t/ha          |plant biomass
        real :: alai = 0.                    !              |leaf area index
        real :: yield = 0.                   !t/ha          |plant yield
        real :: npp = 0.                     !t/ha          |net primary productivity
        real :: lai_mx = 0.                  !              |maximum leaf area index
        real :: gwdeep = 0.                  !mm            |deep aquifer storage
        real :: aet = 0.                     !mm            |sum of actual et during growing season (for hi water stress)
        real :: pet = 0.                     !mm            |sum of potential et during growing season (for hi water stress)
      end type swatdeg_hru_dynamic
      ! tdc 2017-04-05
      !type (swatdeg_hru_dynamic), dimension (:), allocatable :: sd
      !type (swatdeg_hru_dynamic), dimension (:), allocatable :: sd_init
    
      ! contains
      ! include 'sd_hru_read.f90'
      ! include 'sd_hru_control.f90'
      ! include 'sd_hru_output.f90'
 
      end module sd_hru_module
    