initial.plt: Plant community initialization - LREW Sub Water
   PCOM_NAME     PLT_CNT    PLT_NAME        IGRO    PHU_MAT    LAI_INI     BM_INI PHU_ACC_INI        POP    YRS_INI    RSD_INI
     pcom001           3
	                            pnut           0    699.000      0.000      0.000       0.000      0.000      0.000   1000.000
								corn           0   1394.000      0.000      0.000       0.000      0.000      0.000   1000.000
								cots           0   1254.000      0.000      0.000       0.000      0.000      0.000   1000.000
     pcom002           1
                                past           0   4000.000      0.000      0.000       0.000      0.009      0.009   3000.000
     pcom003           1        
                                frse           1   5340.300      0.000      0.000       0.000      0.000      30.00  10000.000
     pcom004           1                                                                                      
                                frsd           1   1826.000      0.000      0.000       0.000      0.000      30.00   1000.000
     pcom005           1                                                                                      
                                wetf           1   5340.300      0.000      0.000       0.000      0.000      30.00  10000.000
     pcom006           1                                                                          
                                watr           0   2256.500      0.000      0.000       0.000      0.000      0.000      0.000
     pcom007           1                                                                          
                                berm           1   1996.000      0.000      0.000       0.000      0.000      0.000   3000.000
