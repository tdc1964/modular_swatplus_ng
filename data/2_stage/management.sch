management.lum: Management schedules - Little River Experimental Watershed 
    NAME  NUM_OPS       OP      MON      DAY     HUSC    COND         OP_TYPE   OP_PLANT  OP_OVER      
   csoy    7  1  irr  autoirr_str.8
                      fert        0        0    0.140    null     anh-nh3-low     null      0.0      FERTILIZER           
                      plnt        0        0    0.150    null            null     corn      0.0      plant CORN BEGIN
                      hvkl       10       30    1.200    null           grain     corn      0.0      HARVKILL
	              skip        0        0    0.000    null            null     null      0.0      SKIP_YEAR 
                      plnt        0        0    0.150    null            null     soyb      0.0      PLANT SOYBEANS
                      hvkl       10       30    1.200    null           grain     soyb      0.0      HARVKILL
	              skip        0        0    0.000    null            null     null      0.0      SKIP_YEAR 
   ryeg    0  0

  