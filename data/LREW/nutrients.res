nutrients.res: Reservoir nutrient inputs - LREW Subbasin March 2016
  RES_NUT_NUMB  RES_NUT_NAME     MID_BEG     MID_END  MID_N_STL      N_STL  MID_P_STL      P_STL    CHLA_CO   SECCI_CO    THETA_N    THETA_P    CONC_NMIN    CONC_PMIN
             1  res_nut_1a           4          10      0.500      2.000      1.000      0.500      1.000      1.000      1.00       1.00       0.10          0.01
             2  res_nut_2a           4          10      0.500      2.000      1.000      0.500      1.000      1.000      1.00       1.00       0.10          0.01
