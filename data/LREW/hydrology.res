hydrology.res: Reservoir properties - LREW Subbasin March 2016
  RES_HYD_NUMB  RES_HYD_NAME       YR_OP      MON_OP    AREA_PS     VOL_PS    AREA_ES     VOL_ES      RES_K    EVAP_CO   SHP_CO_1   SHP_CO_2   VOL_A_CO    FRAC_IN
             1  res_hyd_1a           0           0    120.000     60.000    400.000    150.000      0.500      0.800      0.000      0.000      1.000      1.000
             2  res_hyd_2a           0           0    120.000     60.000    400.000    150.000      0.500      0.800      0.000      0.000      1.000      1.000